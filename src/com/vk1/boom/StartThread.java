package com.vk1.boom;

import android.content.Intent;
import android.graphics.*;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;

import java.io.*;
import java.util.ArrayList;

import static android.graphics.Color.*;

public class StartThread
{
    private boolean running;
    private SurfaceHolder surfaceHolder;
    private int ScreenId;
    private Bitmap Bomb, Fire, Explosion;
    private Point ScrSize;
    private Paint paint;
    private float scale;
    private ArrayList points;
    private MyTime time;
    private int PointsToDraw;
    private float ExpScale;
    private float clr;
    private GraphicsView view;
    private MediaPlayer mPlayer;
    private InputStream RawFile;

    StartThread(SurfaceHolder surfaceHolder, GraphicsView view)
    {
        this.surfaceHolder = surfaceHolder;
        Bomb = BitmapFactory.decodeResource(view.getResources(), R.drawable.bomb);
        Fire = BitmapFactory.decodeResource(view.getResources(), R.drawable.fire);
        Explosion = BitmapFactory.decodeResource(view.getResources(), R.drawable.explosion);
        RawFile = view.getResources().openRawResource(R.raw.points);

        ScrSize = new Point(surfaceHolder.getSurfaceFrame().width(), surfaceHolder.getSurfaceFrame().height());
        paint = new Paint();
        paint.setTextSize(40);
        paint.setARGB(255, 255, 255, 255);
        paint.setStrokeWidth(8);
        paint.setStyle(Paint.Style.STROKE);
        if (ScrSize.x < ScrSize.y)
            scale = (float)ScrSize.x / 800f;
        else
            scale = (float)ScrSize.y / 800f;
        points = new ArrayList<Point>();
        LoadPoints();
        ScreenId = 0;
        ExpScale = 0.2f;
        this.view = view;
        mPlayer = MediaPlayer.create(view.main, R.raw.tick);
        mPlayer.setLooping(true);
        running = true;
    }

    private Point ToPoint(String s)
    {
        int x = 0, y = 0, i, j;
        for (i = 0; i < s.length() && s.charAt(i) != ' '; ++i)
            if (s.charAt(i) != '-')
                x = (x * 10 + s.charAt(i) - '0');
        for (j = ++i; i < s.length(); ++i)
            if (s.charAt(i) != '-')
            y = (y * 10 + s.charAt(i) - '0');
        if (s.charAt(0) == '-')
            x = -x;
        if (s.charAt(j) == '-')
            y = -y;
        return new Point(x, y);
    }

    private void LoadPoints()
    {
        BufferedReader buffreader = new BufferedReader(new InputStreamReader(RawFile));
        String line;
        try {
            while (( line = buffreader.readLine()) != null) {
                points.add(ToPoint(line));
            }
        } catch (IOException e) {}
        PointsToDraw = points.size();
    }

    private void DrawPath(Canvas canvas)
    {
        Path path = new Path();
        if (points.size() > 0)
            path.moveTo(((Point)points.get(0)).x, ((Point)points.get(0)).y);
        for (int i = 1; i < PointsToDraw - 1; i++)
        {
            Point p = (Point)points.get(i);
            path.lineTo(p.x, p.y);
        }
        canvas.drawPath(path, paint);
    }

    private void DrawFire(Canvas canvas)
    {
        Point Fpos = new Point((Point)points.get(PointsToDraw - 1));
        Matrix Fmat = new Matrix();

        Fmat.setTranslate(Fpos.x - Fire.getWidth() / 2,
                Fpos.y - Fire.getHeight() / 1.65f);
        Fmat.postScale(100f / Fire.getWidth(), 100f / Fire.getHeight(),
                Fpos.x, Fpos.y);
        Point A = (Point)points.get(PointsToDraw - 2), B = (Point)points.get(PointsToDraw - 1);
        Fmat.postRotate(90 + (float)Math.atan2(B.y - A.y, B.x - A.x) / (float)Math.PI * 180f, Fpos.x, Fpos.y);
        canvas.drawBitmap(Fire, Fmat, paint);
    }

    private void DrawExpl(Canvas canvas)
    {
        Matrix Emat = new Matrix();
        Emat.setTranslate(-Explosion.getWidth()/ 2,
                -Explosion.getHeight() / 1.9f);
        Emat.postScale(ExpScale, ExpScale,
                0, 0);
        canvas.drawBitmap(Explosion, Emat, paint);
    }

    private void Draw(Canvas canvas)
    {
        switch (ScreenId)
        {
            case 0:
                DrawPath(canvas);
                DrawFire(canvas);
                canvas.drawBitmap(Bomb,
                        new Rect(0, 0, Bomb.getWidth(), Bomb.getHeight()),
                        new Rect(-400, -200, 200, 400), paint);
                break;
            case 1:
                DrawExpl(canvas);
                break;
            case 2:
                for (int i = 0; i < 2; i++)
                {
                    paint.setStyle(i == 0 ? Paint.Style.FILL : Paint.Style.STROKE);
                    paint.setARGB(255, 255, 255 * i, 0);
                    paint.setTextSize(180);
                    canvas.drawText("Тик...Так", -400, -100, paint);
                    paint.setTextSize(360);
                    canvas.drawText("Бумм", -400, 200, paint);
                }
                canvas.drawARGB((int)clr, 255, 255, 255);
                break;
        }
    }

    private void Refresh()
    {
        time.Refresh();
        if (ScreenId == 0 && PointsToDraw > 10)
            PointsToDraw = (int)Math.min(PointsToDraw, points.size() - Math.max(0, time.FromStart / 30 - 20));
        else if (ScreenId == 0)
        {
            ScreenId = 1;
            time = new MyTime();
            mPlayer.stop();
            mPlayer = MediaPlayer.create(view.main, R.raw.boom);
            mPlayer.start();
        }
        else if (ScreenId == 1 && ExpScale < 7)
        {
            ExpScale += time.FromStartS;
        }
        else if (ScreenId == 1)
        {
            time = new MyTime();
            ScreenId = 2;
            clr = 255;
        }
        else if (time.FromStartS < 5)
        {
            clr = Math.max(0, clr - time.FromStartS * 2);
        }
        else
        {
            running = false;
            mPlayer.stop();
            mPlayer.release();
        }
    }

    public void run()
    {
        Canvas canvas;
        time = new MyTime();
        mPlayer.start();
        while (running)
        {
            canvas = null;
            try
            {
                Refresh();
                canvas = surfaceHolder.lockCanvas(surfaceHolder.getSurfaceFrame());
                if (canvas == null)
                    continue;
                canvas.drawColor(BLACK);
                float shiftX = canvas.getWidth() / 2f;
                float shiftY = canvas.getHeight() / 2f;
                canvas.translate(shiftX, shiftY);
                canvas.scale(scale, scale);
                Draw(canvas);
            }
            finally
            {
                if (canvas != null)
                    surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public boolean Touch(View v, MotionEvent event)
    {
        Point tch = new Point((int)((event.getX() - ScrSize.x / 2) / scale), (int)((event.getY() - ScrSize.y / 2) / scale));
        if (points.size() == 0 || !points.get(points.size() - 1).equals(tch)) {
            points.add(tch);
            Log.w("", tch.toString());
            PointsToDraw = points.size();
        }
        return true;
    }

}
