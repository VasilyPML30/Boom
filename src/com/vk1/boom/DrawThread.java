package com.vk1.boom;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;

public class DrawThread extends Thread
{
    protected boolean running = false;
    protected SurfaceHolder surfaceHolder;
    public GraphicsView view;
    private StartThread st;
    protected MenuThread mt;
    protected GameThread gt;
    private int mode = 0;

    public DrawThread(SurfaceHolder surfaceHolder, GraphicsView view)
    {
        this.surfaceHolder = surfaceHolder;
        this.view = view;
        st = new StartThread(surfaceHolder, view);
        mt = new MenuThread(surfaceHolder, view);
        gt = new GameThread(surfaceHolder, view);
    }

    public void setRunning(boolean running) {
        this.running = running;

    }
    public boolean onTouch(View v, MotionEvent event)
    {
        switch (mode) {
            case 1:
                return mt.onTouch(v, event);
            case 2:
                return gt.onTouch(v, event);
        }
        return true;
    }

    public void run()
    {
        st.run();
        mode = 1;
        if (mt.run()) {
            gt.run();
            mode = 2;
        }
        else {
            Handler mainHandler = new Handler(view.main.getMainLooper());
            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    ((ViewGroup) view.getParent()).removeView(view);
                    view.main.onBackPressed();
                }
            };
            mainHandler.post(myRunnable);
        }
    }
}
