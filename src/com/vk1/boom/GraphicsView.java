package com.vk1.boom;

import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

class GraphicsView extends SurfaceView implements SurfaceHolder.Callback, SurfaceView.OnTouchListener
{
    protected DrawThread drawThread = null;
    public Main main;

    public GraphicsView(Context context, Main main)
    {
        super(context);
        getHolder().addCallback(this);
        this.main = main;
    }

    public void surfaceCreated(SurfaceHolder holder)
    {
        if (drawThread == null)
        {
            drawThread = new DrawThread(getHolder(), this);
            drawThread.setRunning(true);
            drawThread.start();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {}
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        boolean retry = true;
        drawThread.setRunning(false);
        while (retry)
        {
            try
            {
                drawThread.join();
                retry = false;
            }
            catch (InterruptedException e){break;}
        }
    }
    public boolean onTouch(View v, MotionEvent event)
    {
        return drawThread.onTouch(v, event);
    }
}