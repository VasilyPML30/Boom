package com.vk1.boom;

import android.graphics.*;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;

import java.io.InputStream;
import java.util.ArrayList;

import static android.graphics.Color.BLACK;

public class GameThread {
    private boolean running;
    private SurfaceHolder surfaceHolder;
    private int ScreenId;
    private Bitmap Bomb, Fire, Explosion;
    private Point ScrSize;
    private Paint paint;
    private float scale;
    private MyTime time;
    private float ExpScale;
    private GraphicsView view;
    private MediaPlayer mPlayer;

    GameThread(SurfaceHolder surfaceHolder, GraphicsView view) {
        this.surfaceHolder = surfaceHolder;
        Bomb = BitmapFactory.decodeResource(view.getResources(), R.drawable.bomb);
        Fire = BitmapFactory.decodeResource(view.getResources(), R.drawable.fire);
        Explosion = BitmapFactory.decodeResource(view.getResources(), R.drawable.explosion);
        ScrSize = new Point(surfaceHolder.getSurfaceFrame().width(), surfaceHolder.getSurfaceFrame().height());
        paint = new Paint();
        paint.setTextSize(40);
        paint.setARGB(255, 255, 255, 255);
        paint.setStrokeWidth(8);
        paint.setStyle(Paint.Style.STROKE);
        if (ScrSize.x < ScrSize.y)
            scale = (float) ScrSize.x / 800f;
        else
            scale = (float) ScrSize.y / 800f;
        ScreenId = 0;
        ExpScale = 0.2f;
        this.view = view;
        mPlayer = MediaPlayer.create(view.main, R.raw.tick);
        mPlayer.setLooping(true);
        running = true;
    }

    public boolean onTouch(View v, MotionEvent event) {
        float x = ((int) event.getX() - ScrSize.x / 2) / scale;
        float y = ((int) event.getY() - ScrSize.y / 2) / scale;
        return true;
    }

    private void Refresh()
    {
        time.Refresh();
    }

    private void Draw(Canvas canvas)
    {
        canvas.drawText(String.valueOf(time.FromStartS), 0, 0, paint);
    }

    public void run()
    {
        Canvas canvas;
        time = new MyTime();
        while (running)
        {
            canvas = null;
            try
            {
                Refresh();
                canvas = surfaceHolder.lockCanvas(surfaceHolder.getSurfaceFrame());
                if (canvas == null)
                    continue;
                canvas.drawColor(BLACK);
                float shiftX = canvas.getWidth() / 2f;
                float shiftY = canvas.getHeight() / 2f;
                canvas.translate(shiftX, shiftY);
                canvas.scale(scale, scale);
                Draw(canvas);
            }
            finally
            {
                if (canvas != null)
                    surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }
}
