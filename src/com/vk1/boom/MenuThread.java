package com.vk1.boom;

import android.graphics.*;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.MAGENTA;

public class MenuThread {
    private SurfaceHolder surfaceHolder;
    private boolean running;
    private Point ScrSize;
    private Paint paint;
    private float scale;
    private GraphicsView view;
    private MyTime time;
    private ArrayList names;
    protected ArrayList players;
    private float listPos;
    private float playersPos;
    private float minLPos, maxLPos, minPPos, maxPPos;
    private float hx, hy;
    private boolean capturedN, capturedP;
    private EditText editBox;
    private Button addButton;
    private int lastAction;
    private Bitmap Bomb, Fire;
    private boolean toContinue;
    private View.OnTouchListener l = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                String newName = editBox.getText().toString();
                if (newName.length() == 0)
                    return true;
                newName = newName.toLowerCase();
                char fst = newName.charAt(0);
                if ('а' <= fst && fst <= 'я')
                    newName = (char) (fst + 'Я' - 'я') + newName.substring(1);
                else if ('a' <= fst && fst <= 'z')
                    newName = (char) (fst + 'Z' - 'z') + newName.substring(1);
                if (names.contains(newName) || newName.equals("Имя"))
                    return true;
                for (int i = 0; i < names.size(); ++i)
                    if (newName.compareTo((String) names.get(i)) < 0) {
                        names.add(i, newName);
                        minLPos = Math.min(-220, 450 - names.size() * 100);
                        editBox.setText("");
                        return true;
                    }
                names.add(newName);
                minLPos = Math.min(-220, 450 - names.size() * 100);
                editBox.setText("");
            }
            return true;
        }
    };

    MenuThread(SurfaceHolder surfaceHolder, GraphicsView view) {
        this.surfaceHolder = surfaceHolder;
        this.view = view;
        ScrSize = new Point(surfaceHolder.getSurfaceFrame().width(), surfaceHolder.getSurfaceFrame().height());
        paint = new Paint();
        paint.setTextSize(70);
        paint.setARGB(255, 255, 255, 255);
        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        if (ScrSize.x < ScrSize.y)
            scale = (float) ScrSize.x / 800f;
        else
            scale = (float) ScrSize.y / 800f;
        editBox = new EditText(view.main);
        editBox.setText("Имя");
        editBox.setSingleLine();
        editBox.setTextSize(17 * scale);
        editBox.setWidth((int) (200 * scale));
        editBox.setX(ScrSize.x / 2 - 600 * scale);
        addButton = new Button(view.main);
        addButton.setTextSize(24 * scale);
        addButton.setText("+");
        addButton.setX(ScrSize.x / 2 - 400 * scale);
        addButton.setY(-10 * scale);
        addButton.setBackgroundColor(BLACK);
        listPos = playersPos = -220;
        ReadNames();
        maxLPos = maxPPos = minPPos = -220;
        minLPos = Math.min(-220, 450 - names.size() * 100);
        capturedN = capturedP = false;
        addButton.setOnTouchListener(l);
        running = true;
        lastAction = -10;
        Bomb = BitmapFactory.decodeResource(view.getResources(), R.drawable.bomb);
        Fire = BitmapFactory.decodeResource(view.getResources(), R.drawable.fire);
    }

    private void ReadNames() {
        Scanner IN = null;
        String name;
        names = new ArrayList();
        players = new ArrayList();
        try {
            IN = new Scanner(new File(view.main.getFilesDir(), "names.txt"));
        } catch (FileNotFoundException e) {
            Log.e("read error", e.toString());
            WriteNames();
            ReadNames();
            return;
        }
        while (IN.hasNext()) {
            name = IN.nextLine();
            names.add(name);
        }
        IN.close();
        if (names.size() == 0) {
            names.add("Вадик");
            names.add("Вася");
            names.add("Лиза");
        }
    }

    private void WriteNames() {
        PrintWriter OUT = null;
        try {
            OUT = new PrintWriter(new File(view.main.getFilesDir(), "names.txt"));
        } catch (FileNotFoundException e)
        {
            Log.e("write error", e.toString());
            return;
        }
        for (int i = 0; i < names.size(); ++i)
            OUT.println(names.get(i));
        OUT.close();
    }

    private void Refresh() {
        time.Refresh();
        if (!capturedN) {
            if (listPos < minLPos)
                listPos += 6 * (minLPos - listPos + 100) * time.DeltaS;
            else if (listPos > maxLPos)
                listPos -= 6 * (listPos - maxLPos + 100) * time.DeltaS;
            if (minLPos - 10 < listPos && listPos < minLPos + 10)
                listPos = minLPos;
            if (maxLPos - 10 < listPos && listPos < maxLPos + 10)
                listPos = maxLPos;
        }
        if (!capturedP) {
            if (playersPos < minPPos)
                playersPos += 6 * (minPPos - playersPos + 100) * time.DeltaS;
            else if (playersPos > maxPPos)
                playersPos -= 6 * (playersPos - maxPPos + 100) * time.DeltaS;
            if (minPPos - 10 < playersPos && playersPos < minPPos + 10)
                playersPos = minPPos;
            if (maxPPos - 10 < playersPos && playersPos < maxPPos + 10)
                playersPos = maxPPos;
        }
    }

    private void Draw(Canvas canvas) {
        for (int i = 0; i < names.size(); ++i) {
            canvas.drawText((String) names.get(i), -600, listPos + i * 100, paint);
            canvas.drawLine((int)(-400f * ScrSize.x / (float) ScrSize.y), listPos + i * 100 - 70, -250, listPos + i * 100 - 70, paint);
        }
        canvas.drawLine((int)(-400f * ScrSize.x / (float) ScrSize.y), listPos + names.size() * 100 - 70, -250, listPos + names.size() * 100 - 70, paint);
        for (int i = 0; i < players.size(); ++i) {
            canvas.drawText(String.valueOf(i + 1) + "." + (String) players.get(i), 280, playersPos + i * 100, paint);
            canvas.drawLine((int)(400f * ScrSize.x / (float) ScrSize.y), playersPos + i * 100 - 70, 250, playersPos + i * 100 - 70, paint);
        }
        canvas.drawLine((int)(400f * ScrSize.x / (float) ScrSize.y), playersPos + players.size() * 100 - 70, 250, playersPos + players.size() * 100 - 70, paint);
        paint.setARGB(255, 0, 0, 0);
        canvas.drawRect((int)(-400f * ScrSize.x / (float) ScrSize.y), -400, (int)(400f * ScrSize.x / (float) ScrSize.y), -300, paint);
        paint.setARGB(255, 255, 255, 255);
        canvas.drawText("Игроки:", 280, -340, paint);
        canvas.drawLine(-250, -400, -250, 400, paint);
        canvas.drawLine(250, -400, 250, 400, paint);

        Matrix StBMatr = new Matrix();
        Matrix StFMatr = new Matrix();
        Matrix ExBMatr = new Matrix();
        Matrix ExFMatr = new Matrix();

        StFMatr.setScale(-100f / Fire.getHeight(), 100f / Fire.getHeight());
        StFMatr.postTranslate(50, -50 - 330);
        StFMatr.postRotate((float)(10 * Math.random() - 5), 0, -310);
        StFMatr.postRotate(time.FromStartS * 30, 0, -170);

        StBMatr.setScale(300f / Bomb.getHeight(), 300f / Bomb.getHeight());
        StBMatr.postTranslate(-150, -150 - 170);
        StBMatr.postRotate(-37 + time.FromStartS * 30, 0, -170);

        ExFMatr.setScale(100f / Fire.getHeight(), 100f / Fire.getHeight());
        ExFMatr.postTranslate(-50, -50 + 10);
        ExFMatr.postRotate((float)(10 * Math.random() - 5), 0, 30);
        ExFMatr.postRotate(-time.FromStartS * 30, 0, 170);

        ExBMatr.setScale(300f / Bomb.getHeight(), 300f / Bomb.getHeight());
        ExBMatr.postTranslate(-150, -150 + 170);
        ExBMatr.postRotate(-37 - time.FromStartS * 30, 0, 170);

        canvas.drawBitmap(Fire, StFMatr, paint);
        canvas.drawBitmap(Bomb, StBMatr, paint);
        canvas.drawBitmap(Fire, ExFMatr, paint);
        canvas.drawBitmap(Bomb, ExBMatr, paint);

        paint.setARGB(255, 0, 255, 0);
        paint.setTextSize(50);
        canvas.drawText("Старт", -70, -160, paint);
        paint.setARGB(255, 255, 0, 0);
        canvas.drawText("Выход", -70, 190, paint);
        paint.setARGB(255, 255, 255, 255);
        paint.setTextSize(70);
    }

    boolean TouchIn(int l, int t, int r, int b, float x, float y)
    {
        return l <= x && x <= r && t <= y && y <= b;
    }

    void AddPlayer(float x, float y)
    {
        int index = (int)((y - listPos + 70) / 100);
        if (0 <= index && index < names.size())
            players.add(names.get(index));
        minPPos = Math.min(-220, 450 - players.size() * 100);
    }
    void DeletePlayer(float x, float y)
    {
        int index = (int)((y - playersPos + 70) / 100);
        if (0 <= index && index < players.size())
            players.remove(index);
        minPPos = Math.min(-220, 450 - players.size() * 100);
    }

    void Next()
    {
        Log.w("menu", "Next pressed");
        toContinue = true;
        running = false;
    }

    void Exit()
    {
        Log.w("menu", "Exit pressed");
        toContinue = false;
        running = false;
    }

    public boolean onTouch(View v, MotionEvent event)
    {
        float x = ((int)event.getX() - ScrSize.x / 2) / scale;
        float y = ((int)event.getY() - ScrSize.y / 2) / scale;

        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                hx = x;
                hy = y;
                if (TouchIn(-150, -300, 150, -50, x, y))
                    Next();
                if (TouchIn(-150, 50, 150, 300, x, y))
                    Exit();
                if (TouchIn((int)(-400f * ScrSize.x / (float) ScrSize.y), -300, -250, 400, x, y)) {
                    capturedN = true;
                }
                else
                    capturedN = false;
                if (TouchIn(250, -300, (int)(400f * ScrSize.x / (float) ScrSize.y), 400, x, y)) {
                    capturedP = true;
                }
                else
                    capturedP = false;
                break;
            case MotionEvent.ACTION_MOVE:
                if (capturedN) {
                    listPos += y - hy;
                    hx = x;
                    hy = y;
                }
                if (capturedP && players.size() > 0) {
                    playersPos += y - hy;
                    hx = x;
                    hy = y;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (TouchIn((int)(-400f * ScrSize.x / (float) ScrSize.y), -300, -200, 400, x, y))
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        AddPlayer(x, y);
                if (TouchIn(200, -300, (int)(400f * ScrSize.x / (float) ScrSize.y), 400, x, y))
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        DeletePlayer(x, y);
                capturedN = capturedP = false;
                break;
        }
        //Log.i("event", String.valueOf(event.getAction()));
        lastAction = event.getAction();
        return true;
    }

    public boolean run()
    {
        Canvas canvas;
        ReadNames();
        WriteNames();
        time = new MyTime();
        Handler mainHandler = new Handler(view.main.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                view.main.addContentView(editBox, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                view.main.addContentView(addButton, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                //((ViewGroup)addButton.getParent()).removeView(addButton);
            }
        };
        mainHandler.post(myRunnable);
        while (running)
        {
            canvas = null;
            try
            {
                Refresh();
                canvas = surfaceHolder.lockCanvas(surfaceHolder.getSurfaceFrame());
                if (canvas == null)
                    continue;
                canvas.drawColor(BLACK);
                float shiftX = canvas.getWidth() / 2f;
                float shiftY = canvas.getHeight() / 2f;
                canvas.translate(shiftX, shiftY);
                canvas.scale(scale, scale);
                Draw(canvas);
            }
            finally
            {
                if (canvas != null)
                    surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
        names.clear();
        WriteNames();
        myRunnable = new Runnable() {
            @Override
            public void run() {
                ((ViewGroup)addButton.getParent()).removeView(addButton);
                ((ViewGroup)editBox.getParent()).removeView(editBox);
            }
        };
        mainHandler.post(myRunnable);
        return toContinue;
    }
}
