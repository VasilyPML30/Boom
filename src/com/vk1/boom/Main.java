package com.vk1.boom;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.*;
import android.widget.Button;
import android.widget.EditText;

import java.io.InputStream;

public class Main extends Activity
{
    private GraphicsView myview;
    protected int mode = 0;
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        myview = new GraphicsView(this, this);
        myview.setOnTouchListener(myview);
        setContentView(myview);
    }
}
